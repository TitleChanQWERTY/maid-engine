
class InputSystem:
     def __init__(self, engine):
          self.Key = engine.input_c
          self.engine_c = engine.engine_c
     
     def GetKeyDown(self, KeyCode):
          return self.engine_c.GetKeyDown(KeyCode)
     def GetKey(self, KeyCode):
          return self.engine_c.GetKey(KeyCode)
     def GetKeyUp(self, KeyCode):
          return self.engine_c.GetKeyUp(KeyCode)
