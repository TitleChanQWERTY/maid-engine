class TransformDynamic:
     def __init__(self, posx, posy, scalex, scaley, rot=0):
          self.position = [posx, posy]
          self.scale = [scalex, scaley]
          self.rotation = rot

          self.sides_position = [posx, posx+scalex, posy, posy+scaley]

     def SetPosition(self, new_posx, new_posy):
          self.position[0] = new_posx
          self.position[1] = new_posy

     def MovePosition(self, move_posx=0, move_posy=0):
          self.position[0] += move_posx
          self.position[1] += move_posy

     def SetRotation(self, new_rotation):
          if (self.rotation > 360 or self.rotation < -360):
               self.rotation = 0
          self.rotation = new_rotation
     def Rotate(self, rotate):
          if (self.rotation > 360 or self.rotation < -360):
               self.rotation = 0
          self.rotation += rotate

class TransformStatic:
     def __init__(self, posx, posy, scalex, scaley, rot=0):
          self.position = [posx, posy]
          self.scale = [scalex, scaley]
          self.rotation = rot
          self.sides_position = [posx, posx+scalex, posy, posy+scaley]
