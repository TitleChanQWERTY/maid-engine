#include "stdio.h"
#include "stdbool.h"

bool isEnterCollider = false;

bool OnColliderEnter(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2)
{
     if (!isEnterCollider)
     {
          if (x1 >= x2 + w2)
          {
               return false;
          }
          if (x1 + w1 <= x2)
          {
               return false;
          }
          if (y1 >= y2 + h2)
          {
               return false;
          }
          if (y1 + h1 <= y2)
          {
               return false;
          }
          isEnterCollider = true;
          return true;
     }
     return false;
}

bool OnColliderStay(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2)
{
     if (isEnterCollider)
     {
          if (x1 >= x2 + w2)
          {
               return false;
          }
          if (x1 + w1 <= x2)
          {
               return false;
          }
          if (y1 >= y2 + h2)
          {
               return false;
          }
          if (y1 + h1 <= y2)
          {
               return false;
          }
          return true;
     }
     return false;
}

bool OnColliderExit(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2)
{
     if (isEnterCollider)
     {
          if (x1 >= x2 + w2)
          {
               isEnterCollider = false;
               return true;
          }
          if (x1 + w1 <= x2)
          {
               isEnterCollider = false;
               return true;
          }
          if (y1 >= y2 + h2)
          {
               isEnterCollider = false;
               return true;
          }
          if (y1 + h1 <= y2)
          {
               isEnterCollider = false;
               return true;
          }
          return false;
     }
     return false;
}
