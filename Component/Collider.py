class Collider:
     def __init__(self, transform, engine):
          self.engine = engine
          self.transform = transform

     def OnColliderEnter(self, object_transform):
          if (object_transform is not False):
               return self.engine.collider_C.OnColliderEnter(object_transform.transform.position[0], 
                                             object_transform.transform.position[1],
                                             object_transform.transform.scale[0],
                                             object_transform.transform.scale[1],
                                             self.transform.position[0],
                                             self.transform.position[1],
                                             self.transform.scale[0],
                                             self.transform.scale[1])
          else:
               return False
     
     def OnColliderStay(self, object_transform):
          if (object_transform is not False):
               return self.engine.collider_C.OnColliderStay(object_transform.transform.position[0], 
                                             object_transform.transform.position[1],
                                             object_transform.transform.scale[0],
                                             object_transform.transform.scale[1],
                                             self.transform.position[0],
                                             self.transform.position[1],
                                             self.transform.scale[0],
                                             self.transform.scale[1])
          else:
               return False

     def OnColliderExit(self, object_transform):
          if (object_transform is not False):
               return self.engine.collider_C.OnColliderExit(object_transform.transform.position[0], 
                                             object_transform.transform.position[1],
                                             object_transform.transform.scale[0],
                                             object_transform.transform.scale[1],
                                             self.transform.position[0],
                                             self.transform.position[1],
                                             self.transform.scale[0],
                                             self.transform.scale[1])
          else:
               return False
