gcc Base/engine.c -o Base/engine.so -O3 -shared -fPIC -I/opt/local/include -L/opt/local/lib $(pkg-config --cflags --libs sdl2) $(pkg-config --cflags --libs SDL2_image)
gcc Component/inputManager.c -o Component/inputManager.so -O3 -shared -fPIC -I/opt/local/include -L/opt/local/lib $(pkg-config --cflags --libs sdl2)
gcc Component/collider.c -o Component/collider.so -O3 -shared -fPIC

if [ $? -eq 0 ]; then
     echo ""
     echo "Compiling .so succeeded. Now you can use .so in python code"
     echo ""
else
     echo ""
     echo "Oh bad. Compiling ERROR!"
     echo ""
fi
