#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>
#include <string.h>

SDL_Window *window;
SDL_Renderer* renderer;

char title[99];
int size[2];

int FPS;

int MAIDENGINE_IMAGE_PNG = IMG_INIT_PNG;
int MAIDENGINE_IMAGE_JPG = IMG_INIT_JPG;
int MAIDENGINE_IMAGE_TIFF = IMG_INIT_TIF;
int MAIDENGINE_IMAGE_WEBP = IMG_INIT_WEBP;

SDL_Event event;

int initEngine(const int flag_img)
{
     if (SDL_Init(SDL_INIT_EVERYTHING) < 0 && !IMG_Init(flag_img))
     {
          return 1;
     }
     else
     {
          window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size[0], size[1], SDL_WINDOW_SHOWN);
          renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
     }
     return 0;
}

bool running = true;

bool isKeyDown = false;
bool isKey = false;
bool isKeyUp = false;

int isKeyDownCount = 0;

SDL_Keycode pressedKey;

void GetEvent()
{
     SDL_PumpEvents();
     while (SDL_PollEvent(&event))
     {
          if (event.type == SDL_QUIT) running = false;

          if (event.type == SDL_KEYDOWN)
          {
               if (isKeyDownCount <= 0) isKeyDown = true;
               isKey = true;
               pressedKey = event.key.keysym.sym;
          }
          else
          {
               isKeyDown = false;
               isKeyDownCount = 0;
               pressedKey = SDLK_UNKNOWN;
               isKey = false;
          }

          if (event.type == SDL_KEYUP)
          {
               isKeyUp = true;
               pressedKey = event.key.keysym.sym;
          }
     }
}

bool checkQuit()
{
     return running;
}

void setFPS(const int fps)
{
     FPS = fps;
}

void setSize(const int w, const int h)
{
     size[0] = w;
     size[1] = h;
     if (window != NULL)
     {
          SDL_SetWindowSize(window, size[0], size[1]);
     }
     
}

void setWindowPosCentered()
{
     SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
}

void setTitle(const char name[])
{
     strcpy(title, name);
}

int StartTime;

void getStartTick()
{
     StartTime = SDL_GetTicks();
}

void tick()
{
     int desiredDelta = 1000 / FPS;
     int delta = SDL_GetTicks() - StartTime;

     if (delta < desiredDelta)
     {
          SDL_Delay(desiredDelta - delta);
     }
}

SDL_Surface* GlobalSurface;
SDL_Texture* GlobalTexture;


void clearScreen()
{
     SDL_FreeSurface(GlobalSurface);
     SDL_DestroyTexture(GlobalTexture);
     SDL_RenderClear(renderer);
}

void updateScreen()
{
     SDL_RenderPresent(renderer);
     tick();
}

void closeGame()
{
     SDL_DestroyRenderer(renderer);
     free(title);
     free(size);
     SDL_DestroyWindow(window);
     IMG_Quit();
     SDL_Quit();
}

SDL_Rect rect;

void setEmptySurface(const int posx, const int posy, const int w, const int h, const int r, const int g, const int b, const int a)
{
     rect.x = posx;
     rect.y = posy;
     rect.w = w;
     rect.h = h;
     GlobalSurface = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0 ,0);
     SDL_FillRect(GlobalSurface, NULL, SDL_MapRGBA(GlobalSurface -> format, r, g, b, a));
     GlobalTexture = SDL_CreateTextureFromSurface(renderer, GlobalSurface);
}

void setImage(const char image_path[], const int posx, const int posy, const int w, const int h)
{
     rect.x = posx;
     rect.y = posy;
     rect.w = w;
     rect.h = h;
     SDL_Rect rect = {posx, posy, w, h};
     GlobalSurface = IMG_Load(image_path);
     GlobalTexture = SDL_CreateTextureFromSurface(renderer, GlobalSurface);
}

void trueRender()
{
     SDL_RenderCopy(renderer, GlobalTexture, NULL, &rect);
}

void rotateSurface(const double angle)
{
     SDL_Point center = {GlobalSurface->w/2, GlobalSurface->h/2};
     SDL_RenderCopyEx(renderer, GlobalTexture, NULL, &rect, angle, &center, SDL_FLIP_NONE);
}

bool isPressed = false;

bool GetKeyDown(const int KeyCode)
{
     if (pressedKey == KeyCode && isKeyDown)
     {
          isKeyDownCount = 1;
          isKeyDown = false;
          return true;
     }
     return false;
}

bool GetKey(const int KeyCode)
{
     if (pressedKey == KeyCode && isKey)
     {
          return true;
     }
     return false;
}

bool GetKeyUp(const int KeyCode)
{
     if (pressedKey == KeyCode && isKeyUp)
     {
          isKeyUp = false;
          return true;
     }
     return false;
}
