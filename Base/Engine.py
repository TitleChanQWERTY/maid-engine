import ctypes
from platform import python_version

MAIDENGINE_PLATFORM_WINDOWS = 1
MAIDENGINE_PLATFORM_UNIX = 2


#⠀⠀⠀⠀⣀⢀⣠⣤⠴⠶⠚⠛⠉⣹⡇⠀⢸⠀⠀⠀⠀⠀⢰⣄⠀⠀⠀⠀⠈⢦⢰⠀⠀⠀⠀⠀⠈⢳⡀⠈⢧⠀⠀⠀⠀⢸⠀⠀⠀⠀
#⠀⠀⠉⠀⠀⠀⡏⠀⢰⠃⠀⠀⠀⣿⡇⠀⢸⡀⠀⠀⠀⠀⢸⣸⡆⠀⠀⠀⠰⣌⣧⡆⠀⢷⡀⠀⠀⣄⢳⠀⠀⢣⠀⠀⠀⢸⠀⠀⠀⠀
#⠀⠀⠀⠀⠀⠀⡇⠀⠘⠀⠀⠀⢀⣿⣇⠀⠸⡇⣆⠀⠀⠀⠀⣿⣿⡀⠀⠀⠀⢹⣾⡇⠀⢸⢣⠀⠀⠘⣿⣇⠀⠈⢧⠀⠀⠘⠀⢠⠀⠀
#⠀⠀⠀⠀⠀⢀⡇⠀⡀⠀⠀⠀⢸⠈⢻⡄⠀⢷⣿⠀⠀⠀⠀⢹⡏⣇⠀⣀⣀⠀⣿⣧⠀⢸⠾⣇⣠⣄⣸⣿⡄⠀⠘⡆⠀⠀⠀⠀⠆⠀
#⠀⠀⠀⠀⠀⣾⢿⠀⠇⠀⠀⠀⢸⠀⠀⢳⡀⢸⣿⡆⠀⠀⠀⣬⣿⡿⠟⠋⠉⠙⠻⣽⣀⡏⠀⠙⠃⢹⡙⡿⣷⠀⠀⢹⠀⠀⠀⠀⠰⠒
#⠀⠀⠀⠀⢸⣿⣿⣇⢸⠀⠀⠀⢸⣦⣤⡀⣷⣸⡟⢧⣀⡴⠶⠿⠻⡄⣀⣤⣴⡾⠖⠚⠿⡀⠀⠀⠀⠈⣧⠁⠹⠆⠀⠀⣇⠀⠀⠀⠀⠀
#⠀⠀⠀⢀⢸⣀⣼⣿⣼⡆⠀⢀⡘⡇⠀⠀⠹⡟⢷⡜⢉⣠⣠⣠⣀⣤⡿⣛⣥⣶⣾⡿⠛⠿⠿⣶⣦⡤⢹⠀⢀⠀⠀⠀⢹⡄⠀⠀⠀⠀
#⠀⠀⠀⢸⢸⡛⠁⠀⠙⢿⠋⠉⠉⠻⠀⠀⠀⢿⣄⠈⠁⠀⠀⠀⢉⢟⣴⡿⠿⠟⢁⠇⠀⠀⠀⠀⠹⣿⠻⡇⢸⠀⠀⠀⠈⣷⠀⠀⠀⠀
#⠀⣀⣀⣘⣿⡇⠀⢀⣠⣤⣶⣶⣶⣾⣦⡀⠀⠈⡿⠀⠀⠀⠀⠀⠀⣿⠟⠳⠦⡤⠊⠀⠀⠀⠀⠀⣸⠇⠀⡇⣼⠀⢰⠀⠀⢹⣇⠀⠀⠀
#⠛⠁⠈⣿⣷⣧⣴⣿⠿⠛⣿⠿⣿⣿⡿⠗⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁⠀⠀⣠⣴⣶⠿⠿⠿⡷⢛⠕⠷⡄⣧⣿⠀⢸⠀⠀⠸⣿⡄⠀⠀
#⠀⠀⢠⣿⢿⣿⣿⠁⠀⠀⠈⠳⠤⠶⠃⠀⠀⢰⡀⠀⠀⠀⠀⠀⠀⠀⣠⣿⣿⠟⣱⠒⡠⢆⡴⣣⣯⢞⣴⡟⢿⡄⡏⠀⠀⠀⡏⢷⡀⠀
#⠀⠀⡌⣿⠀⠙⣿⡦⢀⣤⡴⣶⠖⣲⠆⢀⠞⠁⠱⠀⠀⠀⠀⠀⠠⣾⠟⠛⡡⠞⠁⢀⡴⢋⢎⣽⡿⣫⠋⠀⠘⢷⠃⡄⠀⠀⡇⠈⣿⡀
#⠀⠀⣇⢹⣦⠀⠼⢃⡾⢋⣶⢃⡼⣹⡳⠃⠊⠀⠀⠀⠀⠀⠀⠀⠀⠁⠀⠀⠀⠈⠠⠋⠀⡰⠋⠀⢘⣇⡇⠀⢠⠟⠀⡇⠀⠀⠀⠀⢹⡵
#⠀⠀⢻⣌⢿⡆⠀⡝⣼⠟⣩⢏⣾⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠞⠀⠀⠀⠀⠈⠀⣠⠏⣠⣾⡇⠀⠀⠀⠀⠘⣷
#⡀⠀⢸⣿⣿⣷⠆⢠⠏⡴⠃⡡⠋⠀⠀⠀⠀⠀⠀⣀⣠⠤⠔⠒⠤⣄⣀⠀⠀⢀⣰⠏⠀⠀⠀⠀⢀⣠⡾⠗⠋⢰⠏⡇⠀⠀⠘⠀⠰⢻
#⣇⠀⠘⣿⣿⣟⠻⣄⡞⠀⠐⠁⠀⠀⠀⠀⠀⣠⠞⣩⣤⣶⣶⣾⣷⣶⣬⣿⣿⣿⡏⠀⠀⠀⠀⠉⠉⠁⠀⠀⠀⢸⡆⡇⠀⠀⠀⠀⠀⠀
#⠹⡄⠀⠹⣿⣿⡄⠀⠉⠉⠀⡀⠀⠀⠈⢻⣾⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⣇⣧⠀⠀⠀⠀⠀⠀
#⠀⣿⢦⣀⠘⢿⣷⡀⠀⠀⡀⢦⠀⠀⠀⠀⠹⣿⣿⠏⠙⢻⣿⡿⠛⠉⠀⠸⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⠀⠀⡆⠀⠀⡀
#⢼⣿⠀⠈⢳⣤⣉⣻⣤⣀⣉⣩⠆⠀⠀⠀⠀⠹⡿⠀⠀⠈⡿⠀⠀⠀⠀⣸⡇⠀⠀⠀⠀⠀⠀⠓⠂⠀⣠⣾⣿⣿⡿⢿⡄⠀⣧⠀⠀⠹
#⣾⠃⠀⣠⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⢸⡇⠀⠀⢠⠴⣿⡄⠀⠀⠀⠀⠀⠀⠀⣠⣾⣿⣿⣿⡿⣧⣀⠧⣰⣻⢄⠀⠀
#⠛⠶⢾⣿⣽⣭⣽⣭⢹⣷⠀⢹⣦⣀⠀⠀⠀⠀⡄⠀⠀⣸⡀⠀⠀⠁⣰⣧⣽⠀⠀⠀⠀⢀⣴⣾⣿⣿⡟⣻⣿⣿⣿⣿⢠⣿⣧⡸⣷⣄
#⠀⠀⠀⠈⠙⠿⣿⣿⣿⠏⠀⣾⣿⣿⣷⣦⣀⠀⢇⠀⠀⠈⠁⠀⣠⠔⠁⠀⠀⠀⠀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠏⣼⣿⠏⣷⡈⠉
#⠀⠀⠀⠀⠀⠀⠀⠙⠻⣶⣾⣿⣿⣿⣿⣿⣿⣷⣾⡆⠀⠀⠀⡾⠁⠀⠀⠀⣀⡴⠞⠛⣛⣿⡿⠿⠛⠛⠉⠉⠀⠀⠀⢰⣿⡿⠂⠈⠻⡄
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢎⠉⠛⠻⠿⠿⠿⠿⠿⣇⠠⠸⣇⣀⣤⣴⣾⡭⠶⠛⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⣿⠇⠀⠀⠀⠘
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠑⣤⡀⠀⠀⠀⠀⠀⠈⣳⠀⣿⠛⠻⠛⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⣿⡯⠀⠀⠀⠀⠀


class Engine:
     def __init__(self, NameWindow=b"Maid Engine - Example Window", Size=(640, 480), FPS=60, select_platform=0):
          self.select_platform = select_platform

          if (self.select_platform == MAIDENGINE_PLATFORM_UNIX):
               self.engine_c = ctypes.CDLL("Base/engine.so")
               self.collider_C = ctypes.CDLL("Component/collider.so")
               self.input_c = ctypes.CDLL("Component/inputManager.so")
          elif (self.select_platform == MAIDENGINE_PLATFORM_WINDOWS):
               self.engine_c = ctypes.CDLL("Base/engine.dll")
               self.collider_C = ctypes.CDLL("Component/collider.dll")
               self.input_c = ctypes.CDLL("Component/inputManager.dll")

          self.engine_c.GetKey.restype = ctypes.c_bool
          self.engine_c.GetKey.argtypes = [ctypes.c_int]

          self.engine_c.setSize.argtypes = [ctypes.c_int, ctypes.c_int]
          self.engine_c.setFPS.argtypes = [ctypes.c_int]
          self.engine_c.setTitle.argtypes = [ctypes.c_char_p]
          self.engine_c.checkQuit.restype = ctypes.c_bool
          self.engine_c.MAIDENGINE_IMAGE_PNG.restype = ctypes.c_int
          self.engine_c.MAIDENGINE_IMAGE_JPG.restype = ctypes.c_int
          self.engine_c.MAIDENGINE_IMAGE_TIFF.restype = ctypes.c_int
          self.engine_c.MAIDENGINE_IMAGE_WEBP.restype = ctypes.c_int
          self.engine_c.rotateSurface.argtypes = [ctypes.c_double]
          self.engine_c.setEmptySurface.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
          self.engine_c.setImage.argtypes = [ctypes.c_char_p, ctypes.c_int, ctypes.c_int, ctypes.c_int]
          self.collider_C.OnColliderEnter.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                       ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
          self.collider_C.OnColliderStay.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                       ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
          self.collider_C.OnColliderExit.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                       ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
          
          self.MAIDENGINE_IMAGE_PNG = self.engine_c.MAIDENGINE_IMAGE_PNG
          self.MAIDENGINE_IMAGE_JPG = self.engine_c.MAIDENGINE_IMAGE_JPG
          self.MAIDENGINE_IMAGE_TIFF = self.engine_c.MAIDENGINE_IMAGE_TIFF
          self.MAIDENGINE_IMAGE_WEBP = self.engine_c.MAIDENGINE_IMAGE_WEBP
          self.select_img_flag = self.MAIDENGINE_IMAGE_PNG

          self.Size = [Size[0], Size[1]]
          self.engine_c.setSize(self.Size[0], self.Size[1])
          self.engine_c.setFPS(FPS)
          self.engine_c.setTitle(NameWindow)

          self.DefaultScene = 0
          self.SelectedScene = 0

          self.Scene = []

          self.GameName = ""
          self.GameVersion = ""
          self.CompanyName = ""
          self.CopyrightInfo = ""

          self.VER = "Maid Engine: DEV"
          self.Copyright = "Copyright 2023-2023 Say!Moon!Studio - TitleChanQWERTY. All Rights Reserved."

     def SetGameInfo(self, GameName="", GameVersion="", CompanyName="", CopyrightInfo=""):
          self.GameName = GameName
          self.GameVersion = GameVersion
          self.CompanyName = CompanyName
          self.CopyrightInfo = CopyrightInfo

     def GetEngineVersion(self):
          return self.VER

     def GetPythonVersion(self):
          return "Python: "+python_version()

     def SetImageFormat(self, format):
          self.select_img_flag = format

     def StartEngine(self):
          if (self.engine_c.initEngine(self.select_img_flag) > 0):
               raise Exception("OH, NO! We were unable to initiate the engine...")
          print(" -> "+self.GetEngineVersion())
          print(" -> "+self.GetPythonVersion())
          print(self.Copyright)
          self.UpdateEngine()
          return

     def UpdateEngine(self):
          running = True
          while running:
               self.engine_c.GetEvent()
               running = self.engine_c.checkQuit()
               self.engine_c.getStartTick()
               self.engine_c.clearScreen()
               if len(self.Scene) > 0:
                    self.Scene[self.SelectedScene].GlobalUpdate()
               self.engine_c.updateScreen()
          self.engine_c.closeGame()

     def SetWindowSize(self, width, height):
          self.Size[0] = width
          self.Size[1] = height
          self.engine_c.setSize(width, height)

     def SetWindowPosCenter(self):
          self.engine_c.setWindowPosCentered()

     def AddScene(self, scene):
          self.Scene.append(scene)

     def DrawEmptySurface(self, transform, color=[255, 0, 255, 255]):
          transform.position[0] = int(transform.position[0])
          transform.position[1] = int(transform.position[1])
          transform.scale[0] = int(transform.scale[0])
          transform.scale[1] = int(transform.scale[1])
          self.engine_c.setEmptySurface(transform.position[0], transform.position[1], transform.scale[0], transform.scale[1], color[0], color[1], color[2], color[3])
     def DrawImage(self, transform, image_path):
          transform.position[0] = int(transform.position[0])
          transform.position[1] = int(transform.position[1])
          transform.scale[0] = int(transform.scale[0])
          transform.scale[1] = int(transform.scale[1])
          self.engine_c.setImage(image_path, transform.position[0], transform.position[1], transform.scale[0], transform.scale[1])
     def RenderObject(self):
          self.engine_c.trueRender()
     def RotationObject(self, angle):
          self.engine_c.rotateSurface(angle)
     def GetScreenSize(self):
          return self.Size
