class Scene:
     def __init__(self, engine):
          self.engine = engine

          self.CurrentObjectTag = 0
          self.SceneObject = []

     def Destroy(self, object):
          self.SceneObject.remove(object)

     def AddToScene(self, object):
          object.SetScene(self, self.engine)
          self.SceneObject.append(object)

     def FindObjectWithTag(self, tag):
          if (self.CurrentObjectTag > len(self.SceneObject)):
               self.CurrentObjectTag = 0
          if (self.SceneObject[self.CurrentObjectTag].tag == tag):
               return self.SceneObject[self.CurrentObjectTag]
          self.CurrentObjectTag += 1
          return False
     
     def SceneUpdate(self):
          pass

     def SceneLateUpdate(self):
          pass

     def GlobalUpdate(self):
          self.SceneUpdate()
          for object in self.SceneObject:
               object.Draw(self.engine)
               object.Update()
               if (object.transform.rotation > 0 or object.transform.rotation < 0):
                    self.engine.RotationObject(object.transform.rotation)
               else:
                    self.engine.RenderObject()
          self.SceneLateUpdate()
