from Base import Engine, Scene
from Component import InputSystem
from WorldObject import Entity, Object

class Wall(Object.Object):
     def __init__(self, posx, posy):
          super().__init__(posx, posy, 45, 25, tag="Wall")

class Player2(Entity.Entity):
     def __init__(self):
          super().__init__(engine.GetScreenSize()[0]/2, 0, 45, 25)
     
     def Update(self):
          self.transform.MovePosition(0, 3)

class Player(Entity.Entity):
     def __init__(self):
          super().__init__(0, 195, 35, 45, tag="Player")
          self.image_path = b"ExampleImage.png"
     def Update(self):
          if (inputsys.GetKeyDown(inputsys.Key.MAIDENGINE_KEY_SPACE())):# type: ignore
               print("SHOOT!")
               self.Scene.AddToScene(Bullet(self.transform.position[0], self.transform.position[1], -1, 0))#type: ignore

          if (inputsys.GetKey(inputsys.Key.MAIDENGINE_KEY_RIGHT())):# type: ignore
               self.transform.MovePosition(5, 0)
          elif (inputsys.GetKey(inputsys.Key.MAIDENGINE_KEY_LEFT())):# type: ignore
               self.Scene.AddToScene(Bullet(self.transform.position[0], self.transform.position[1], -1, 0))#type: ignore
          
          if (inputsys.GetKey(inputsys.Key.MAIDENGINE_KEY_UP())):# type: ignore
               self.transform.MovePosition(0, -5)
          elif (inputsys.GetKey(inputsys.Key.MAIDENGINE_KEY_DOWN())):# type: ignore
               self.transform.MovePosition(0, 5)


class Bullet(Entity.Entity):
     def __init__(self, posx, posy, move_x, move_y):
          super().__init__(posx, posy, 15, 15, 0, (255, 255, 255, 255))
          self.move_x = move_x
          self.move_y = move_y
     def Update(self):
          self.transform.MovePosition(self.move_x, self.move_y)
          if (self.transform.position[0] <= 5):
               self.Remove(self)


class Scene1(Scene.Scene):
     def __init__(self, engine):
          super().__init__(engine)
          self.AddToScene(Player2())
          self.AddToScene(Player())
          self.AddToScene(Wall(195, 205))
     
engine = Engine.Engine(select_platform=Engine.MAIDENGINE_PLATFORM_UNIX)
inputsys = InputSystem.InputSystem(engine)
scene1 = Scene1(engine)
engine.AddScene(scene1)
engine.StartEngine()
