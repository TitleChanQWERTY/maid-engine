from Component import Transform, Collider

class Object:
     def __init__(self, posx, posy, scalex, scaley, rotate=0, color=(255, 0, 255, 255), tag="Empty"):
          self.Scene = None
          self.image_path = None
          self.transform = Transform.TransformStatic(posx, posy, scalex, scaley, rotate)
          self.collider = None
          self.color = color

          self.tag = tag

          self.engine = None
     
     def SetScene(self, scene, engine):
          self.Scene = scene
          self.engine = engine
          self.collider = Collider.Collider(self.transform, engine)

     def Remove(self, object):
          self.Scene.Destroy(object)#type: ignore

     def Draw(self, engine):
          if self.image_path is None:
               engine.DrawEmptySurface(self.transform, self.color)
          else:
               engine.DrawImage(self.transform, self.image_path)
     
     def Update(self):
          pass
